# Introduction

This repo provides some tools to explore the [SEN1-2MS](https://arxiv.org/abs/1906.07789) dataset from TUM. SEN1-2MS contains around 190k coregistered Sentinel 1 and 2 images from all over the world, along with MODIS-derived 500m landcover classification. Each image is 256 x 256px.

The data are clean: cloudy images have been removed, along with stitching artifacts.

The `sen12ms` library here provides a PyTorch dataloader.

# Requirements

You can grab the requirements using `pip install -r requirements.txt`. Preferably do this inside a virtualenv or a conda environment.

For example:

```
cd sen12ms
conda create -n fdl python=3.7
conda install jupyter notebook
pip install -r requirements.txt
pip install .
```

This is preferable to using `setup.py` directly as it makes uninstalling much easier. Also be sure to install jupyter notebook within your new conda env, otherwise weird stuff happens.

Images are loaded using `rasterio` and PyTorch is used for the ML. 

# Dataset

## Getting the dataset

There are two ways of getting the dataset.

### 1. From TUM
Download the dataset from: http://mediatum.ub.tum.de/1474000

Then extract the data:
```
tar -xvf *.tar.gz
```

### 2. From Google Cloud Storage
```
gsutil -m rsync -r gs://fdl_floods_2019_data/SEN12MS .

```

Assuming you've put your data in `/mnt/data` (you can modify the path in the notebook).

Have a look at the [data visualisation notebook](https://gitlab.com/frontierdevelopmentlab/disaster-prevention/sen12ms/blob/master/notebooks/dataloader_example.ipynb) to get started.


### Dataset mean/std

If you downloaded the data from GCP, then you will also have a file which you can use to compute the mean and standard deviation of the dataset for normalisation.

Otherwise, you need to compute this manually (see the notebooks).

# Training

Again, check where you've extracted the SEN1-2MS dataset, then to train a basic linear classifier, run:

`python train_linear.py`

You should also modify the number of CPU cores used for data loading.
