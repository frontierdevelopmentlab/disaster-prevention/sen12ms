from setuptools import setup, find_packages

setup(
    name='sen12ms',
    version='0.0.1',
    url='https://gitlab.com/frontierdevelopmentlab/fdl-europe-2019/disaster-prevention/sen12ms.git',
    author='Josh Veitch-Michaelis',
    author_email='j.veitchmicahelis@gmail.com',
    description='SEN1-MS toolkit',
    install_requires=['rasterio', 'natsort'],
    packages=find_packages()
)
