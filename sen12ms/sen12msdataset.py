#!/usr/env/bin python3

import torch.utils.data
import rasterio
import numpy as np
from pathlib import PosixPath
import os
from natsort import natsorted
from glob import glob


class Sen12MSDataset(torch.utils.data.Dataset):
    def __init__(self, path, rgb=False, sar=False, limit=None, transforms=None):
        """
        Args:
           path: Path to extracted SEN12MS data
           rgb: (bool) whether to only return RGB images
           sar: (bool) whether to include SAR
           limit: (int) how many images to load
        """
        path = PosixPath(path)

        optical_path = os.fspath(path/"ROIs*_*/s2*/*.tif")
        sar_path = os.fspath(path/"ROIs*_*/s1*/*.tif")
        landcover_path = os.fspath(path/"ROIs*_*/lc*/*.tif")

        self.load_sar = sar

        self.optical_images = natsorted(glob(optical_path))
        self.sar_images = natsorted(glob(sar_path))
        self.landcover_images = natsorted(glob(landcover_path))

        if limit is not None:
            self.optical_images = self.optical_images[:limit]
            self.sar_images = self.sar_images[:limit]
            self.landcover_images = self.landcover_images[:limit]

        assert(len(self.optical_images) > 0)
        assert(len(self.optical_images) == len(self.sar_images))
        assert(len(self.optical_images) == len(self.landcover_images))

        self.export_rgb = rgb
        #self.c = 20
        self.output_one_hot = False
        self.transform = transforms

    def __len__(self):
        return len(self.optical_images)

    def __getitem__(self, idx):
        """
        Returns:
           x: Satellite image stack (sar_vv, sar_vh, sentinel_2)
           y: IGBP landcover segmentation map
        """
        # Sentinel 1 Data
        sar_vv = rasterio.open(self.sar_images[idx]).read(1).astype('float32')
        sar_vh = rasterio.open(self.sar_images[idx]).read(2).astype('float32')

        sar_vv = np.abs(np.nan_to_num(sar_vv))
        sar_vh = np.abs(np.nan_to_num(sar_vh))

        # Sentinel 2 (optional only export RGB bands)
        optical = rasterio.open(
            self.optical_images[idx]).read().astype('float32')

        # Landcover segmentation
        landcover_igbp = rasterio.open(
            self.landcover_images[idx]).read(1).astype('int')

        if self.output_one_hot:
            landcover_igbp[landcover_igbp > 17] = 0

            mask = np.zeros((18, 256, 256), dtype='bool')

            for i in range(18):
                mask[i] = (landcover_igbp == i)

            mask = mask.astype('float32')
        else:
            mask = landcover_igbp.astype('float32')

        y = mask

        x = np.concatenate((sar_vv.reshape((1, 256, 256)),
                            sar_vh.reshape((1, 256, 256)), optical))

        if self.transform is not None:
            res = self.transform(image=x.transpose(1, 2, 0), mask=y)
            x, y = res['image'], res['mask']

        # If we only want RGB
        if self.export_rgb:
            x = x[(0, 1, 3, 4, 5)]

        if not self.load_sar:
            x = x[2:]

        return x, y


if __name__ == "__main__":
    print("Testing dataset")
    import sys

    path = "/mnt/data"
    if len(sys.argv) > 1:
        path = sys.argv[1]

    dataset = Sen12MSDataset(path)
